#!/bin/bash
# se detiene la ejecución del script ante cualquier tipo de error
#set -e


# Actualización de bases de datos e instalción de apache y git
apt update && apt install apache2 git -y && apt clean

# directorio donde se clonara el repo

mkdir /app

# clonamos el repositorio con protolo https para que no pida login, 
# en este modo el repo debe ser publico

if git clone https://gitlab.com/sergio.pernas2/bootstrap_template.git /app;then

	cp -r /app/* /var/www/html
else
	# si el repositorio ya esta clonado hacemos un pull
	# para traer eventuales actualizacion de la pagina
	cd /app && git pull && cp -r /app/* /var/www/html

fi


exec "$@" # ejecutamos todo lo que venga como paramentro (CMD del docker file)

