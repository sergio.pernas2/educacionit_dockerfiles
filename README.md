# Git y Gitlab

Git es un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de aplicaciones cuando estas tienen un gran número de archivos de código fuente. Su propósito es llevar registro de los cambios en archivos de computadora incluyendo coordinar el trabajo que varias personas realizan sobre archivos compartidos en un **repositorio de código**.

1. Crear una cuenta en 'gitlab'.

2. Crear llave ssh en la computadora que trabajara los repositorios.

En el cliente (linux) generar un juego de llaves publico y privado.

```bash
$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/educacionit/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/educacionit/.ssh/id_rsa
Your public key has been saved in /home/educacionit/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:f2CTjRc/VURDuLc40JcjRdiuzw9qALan7od1RP4ELmY educacionit@athena
...

$ ls .ssh/
id_rsa id_rsa.pub known_hosts
```

3. Agregar llave publica a nuestro perfil de gitlab.

```bash
$ cat .ssh/id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDaXiG3NwDYUObY0auPDsZQQvmPWdSohHsiIUevRWzPDXqJlfjRuCi1YxZYfBB9FEp1u/ipOs8wPhqfyngSbshA3QvroD856hmHc7/ca/QmV4oqdA0/7L1o9JeJPJ4ONZ7awgxHI0FX046jsyT6xZVTNRMHFrTpsX8/skQ7BQuYd3GjFh5WCsgg3rxPKFNi80WpGfJeqgsqZrqATaY8aZdsdgljffmaCcNeSX/OaWdMOFdO77oQOFhtwSm83Ovg0DfD9U18T8ySb58kn5nE+TuSAfZFjtEZG6hoIytiFp6sJ6mgNAdyV8qJ0/mp65EifbLvrgKpG2AL2gFMGOg04UbCDrR1BmPPmHfC2NgGECzp6S8sYIz+sgkoLGKHFqNhkKy29TmOLDQiVMzN3hcQi0mnk92fd4OsnZZ1QuNXz/a+Np3ZocSU8AG6dTU2S7h7XSu6Xw9TWRTk9G+CKr4dFbm6dHWw7YtekJw6OU0pMI5iksOLTtRKpu2pQtfZx46urQLgR2lfsc= xxxxxx@xxxxx
```

Copiar el contenido en la configuracion de llaves de gitlab.

4. Crear repositorio.

Crear un nuevo repositorio en 'gitlab':

​    \+ > Project/repository > blank project:

​    

​    Name: bootstrap_template

​    Visibility Level: public

​    Project Configuration: Initialize repository with a README

​    **Create project**

​    

5. Trabajar con el repositorio

En la computadora cliente clonar el repo bajo el protocolo 'ssh'.

```
$ git clone git@gitlab.com:sergio.pernas2/bootstrap_template.git
```

Dentro del repositorio que clonamos, ahora repositorio local, agregamos y/o creamos los archivos que contendra dicho respositorio, una vez terminado esto debemos actualizar el remoto respecto del local.

Dentro del repositorio local añadir los ficheros nuevos, eliminados y/o modificados.

 ```
$ git add .
 ```

Crear un 'commit'.

```
$ git commit -m "actualizado"
[main 2a84af9] actualizado
 11 files changed, 529 insertions(+)
 create mode 100644 assets/brand/bootstrap-logo-white.svg
 create mode 100644 assets/brand/bootstrap-logo.svg
 create mode 100644 assets/dist/css/bootstrap.min.css
 create mode 100644 assets/dist/css/bootstrap.min.css.map
 create mode 100644 assets/dist/css/bootstrap.rtl.min.css
 create mode 100644 assets/dist/css/bootstrap.rtl.min.css.map
 create mode 100644 assets/dist/js/bootstrap.bundle.min.js
 create mode 100644 assets/dist/js/bootstrap.bundle.min.js.map
 create mode 100644 blog.css
 create mode 100644 blog.rtl.css
 create mode 100644 index.html
```

Actualizar repositorio remoto con respecto del local.

```
$ git push
Enter passphrase for key '/home/educacionit/.ssh/id_rsa': 
Enumerando objetos: 19, listo.
Contando objetos: 100% (19/19), listo.
Compresión delta usando hasta 6 hilos
Comprimiendo objetos: 100% (18/18), listo.
Escribiendo objetos: 100% (18/18), 238.30 KiB | 5.54 MiB/s, listo.
Total 18 (delta 3), reusado 0 (delta 0)
To gitlab.com:sergio.pernas2/bootstrap_template.git
```



# Creación de imagenes.

Con Docker es posible crear imagenes personalizadas a partir de imganes previas, la creacion de imagnes se hace mediante un fichero llamado 'Dockerfile' este fichero es un script que le dice a docker que debe colocar dentro de la imagen.

En un sistema con Docker.

## Imagen 'basic_html-v2': Solo Dockerfile

Directorio de trabajo 'basic_html-v1'.

#### Fichero 'Dockerfile'

```dockerfile
# imagen base para la creación del contenedor temporal
FROM ubuntu

# la instruccion 'RUN' ejecuta un comando, que exista, dentro del contenedor temporal
RUN apt update && apt install -y apache2 && apt clean

# copiar un fichero al directorio '/var/www/html' del contenedor temporal
COPY index.html /var/www/html

# exponer o 'abrir puertos' del contenedor
EXPOSE 80

# comando o programa a ejecutar cuando se lanze
# un contendor a partir de esta nueva imagen
CMD ["apache2ctl", "-D", "FOREGROUND"]
```



#### Fichero 'index.html'

```html
<h1>Este contenedor se lanzo a partir de una imagen personalizada.</h1>
```



#### Creacion de la imagen

```
$ docker build -t basic_html-v1 .
...
 ---> 484b8bb039ad
Step 5/5 : CMD ["apache2ctl", "-D", "FOREGROUND"]
 ---> Running in 64f08a2f19db
Removing intermediate container 64f08a2f19db
 ---> 1e07fdef5a15
Successfully built 1e07fdef5a15
Successfully tagged basic_html-v1:latest
```



#### Listar las imagenes existentes en el sistema

```
$ docker images
REPOSITORY    TAG    IMAGE ID    CREATED       SIZE
>> basic_html-v1  latest  1e07fdef5a15  About a minute ago  225MB
ubuntu/apache2  latest  e1b1f42cd489  25 hours ago     179MB
nginx      latest  88736fe82739  11 days ago     142MB
ubuntu      latest  a8780b506fa4  3 weeks ago     77.8MB
```



#### Lanzar contenedor con la nueva imagen

Omitimos la opción `-d` para ver la salida de los procesos que se ejecutan en el contenedor.

```
$ docker run \
--name basic_html-v1-container \
-p 9095:80 \
basic_html-v1
```



## Imagen 'basic_html-v2': Dockerfile + entrypoint

Directorio de trabajo 'basic_html-v2'.

#### Fichero 'Dockerfile'

```dockerfile
# imagen base para la creación del contenedor temporal
FROM ubuntu

# copiar script de deploy 'entrypoint.sh' al directorio raiz del contenedor
COPY entrypoint.sh /

# permisos de ejeucion
RUN chmod +x /entrypoint.sh

# exponer o 'abrir puertos' del contenedor
EXPOSE 80

# comando o programa a ejecutar cuando se
# lanze un contendor a partir de esta nueva imagen
ENTRYPOINT ["/entrypoint.sh"]

# CMD actua como paramentros o argumentoas que se le pasan al script ENTRYPOINT
CMD ["apache2ctl", "-D", "FOREGROUND"]
```



#### Fichero 'entrypoint.sh'

```bash
#!/bin/bash
# se detiene la ejecución del script ante cualquier tipo de error
set -e

# Actualización de bases de datos e instalción de apache
apt update && apt install apache2 wget -y && apt clean

# descargar pagina de prueba
wget -O /var/www/html/index.html \
https://gitlab.com/sergio.pernas2/educacionit_dockerfiles/-/raw/main/demo.html

# ejecutamos todo lo que venga como paramentro (CMD del docker file)
exec "$@"
```



#### Creacion de la imagen

```
$ docker build -t basic_html-v2 .
```



#### Lanzar contenedor con la nueva imagen

```
$ docker run \
--name basic_html-v2-container \
-p 9096:80 \
basic_html-v2
```



## Imagen 'bootstrap_template': Dockerfile + entrypoint + repositorio git

Directorio de trabajo 'bootstrap_template'.

#### Fichero 'Dockerfile'

```dockerfile
# imagen base para la creación del contenedor temporal
FROM ubuntu

# copiar script de deploy 'entrypoint.sh' al directorio raiz del contenedor
COPY entrypoint.sh /

# permisos de ejeucion
RUN chmod +x /entrypoint.sh

# exponer o 'abrir puertos' del contenedor
EXPOSE 80

# comando o programa a ejecutar cuando se
# lanze un contendor a partir de esta nueva imagen
ENTRYPOINT ["/entrypoint.sh"]

# CMD actua como paramentros o argumentoas que se le pasan al script ENTRYPOINT
CMD ["apache2ctl", "-D", "FOREGROUND"]
```



#### Fichero 'entrypoint.sh'

```bash
#!/bin/bash
# se detiene la ejecución del script ante cualquier tipo de error
#set -e

# Actualización de bases de datos e instalción de apache y git
apt update && apt install apache2 git -y && apt clean

# directorio donde se clonara el repo
mkdir /app

# clonamos el repositorio con protolo https para que no pida login, 
# en este modo el repo debe ser publico
if git clone https://gitlab.com/sergio.pernas2/bootstrap_template.git /app;then
	cp -r /app/* /var/www/html
else
	# si el repositorio ya esta clonado hacemos un pull
	# para traer eventuales actualizacion de la pagina
	cd /app && git pull && cp -r /app/* /var/www/html
fi
# ejecutamos todo lo que venga como paramentro (CMD del docker file)
exec "$@"
```



#### Creacion de la imagen

```
$ docker build -t bootstrap_template .
```



#### Lanzar contenedor con la nueva imagen

```
$ docker run \
--name bootstrap_template-container \
-p 9097:80 \
bootstrap_template
```






