#!/bin/bash
# se detiene la ejecución del script ante cualquier tipo de error
set -e

# Actualización de bases de datos e instalción de apache
apt update && apt install apache2 wget -y && apt clean

# descargar pagina de prueba
wget -O /var/www/html/index.html \
https://gitlab.com/sergio.pernas2/educacionit_dockerfiles/-/raw/main/demo.html

# ejecutamos todo lo que venga como paramentro (CMD del docker file)
exec "$@"
